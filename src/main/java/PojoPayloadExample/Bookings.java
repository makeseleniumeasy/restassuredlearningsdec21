package PojoPayloadExample;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bookings {
	
	private String tripType = "One Way";
	private String departFrom = "Mumbai";
	private String goingTo = "Delhi";
	private String departureDate = "25-01-2021";
	
	private List<Travellers> travellers;
	
	private  Payment   payment;
	
	
	
	
	

}
