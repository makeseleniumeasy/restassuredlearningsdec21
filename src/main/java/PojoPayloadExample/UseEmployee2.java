package PojoPayloadExample;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UseEmployee2 {
	
	public static void main(String[] args) throws JsonProcessingException {
		
		
		Employee employee = new Employee();
		employee.setFirstName("AMod");
		//semployee.setLastName("Mahajan");
		//employee.setAge(28);
		employee.setMarried(false);
		employee.setSalary(28.34);
		
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		String jsonEMployee = objectMapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(employee);
		System.out.println(jsonEMployee);
	}

}
