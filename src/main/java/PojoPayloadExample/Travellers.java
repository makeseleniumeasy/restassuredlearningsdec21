package PojoPayloadExample;

public class Travellers {

	private String firstName;
	private String lastName;
	private int age;
	private boolean coronaCertificatte;
	
	
	public boolean isCoronaCertificatte() {
		return coronaCertificatte;
	}
	public void setCoronaCertificatte(boolean coronaCertificatte) {
		this.coronaCertificatte = coronaCertificatte;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
