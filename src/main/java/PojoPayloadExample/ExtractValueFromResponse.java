package PojoPayloadExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExtractValueFromResponse {

	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		
		String response = "{\r\n" + 
				"  \"tripType\": \"One Way\",\r\n" + 
				"  \"departFrom\": \"London\",\r\n" + 
				"  \"goingTo\": \"Delhi\",\r\n" + 
				"  \"departureDate\": \"25-01-2021\",\r\n" + 
				"  \"travellers\": [\r\n" + 
				"    {\r\n" + 
				"      \"firstName\": \"Amod\",\r\n" + 
				"      \"lastName\": \"Mahajan\",\r\n" + 
				"      \"age\": 29,\r\n" + 
				"      \"coronaCertificatte\": false\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"firstName\": \"Rahul\",\r\n" + 
				"      \"lastName\": \"Arora\",\r\n" + 
				"      \"age\": 35,\r\n" + 
				"      \"coronaCertificatte\": false\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"payment\": {\r\n" + 
				"    \"carNo\": 32423423,\r\n" + 
				"    \"cvv\": 234,\r\n" + 
				"    \"accountName\": \"AMod\"\r\n" + 
				"  }\r\n" + 
				"}";
		
		ObjectMapper objectMapper = new ObjectMapper();
		Bookings bookings = objectMapper.readValue(response, Bookings.class);
		System.out.println(bookings.getDepartFrom());
		System.out.println(bookings.getDepartureDate());
		
		System.out.println(bookings.getPayment().getAccountName());
		System.out.println(bookings.getPayment().getCarNo());
		
		System.out.println(bookings.getTravellers().size());
		
		System.out.println(bookings.getTravellers().get(0).getFirstName());
		
		Payment payment = bookings.getPayment();
		System.out.println(payment.getAccountName());
		System.out.println(payment.getCarNo());
	}
}
