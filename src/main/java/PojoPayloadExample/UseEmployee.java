package PojoPayloadExample;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UseEmployee {
	
	public static void main(String[] args) throws JsonProcessingException {
		
		
		Employee employee = new Employee();
		employee.setFirstName("AMod");
		employee.setLastName("Mahajan");
		employee.setAge(28);
		employee.setMarried(false);
		employee.setSalary(28.34);
		
		Employee employee1 = new Employee();
		employee1.setFirstName("hh");
		employee1.setLastName("kj");
		employee1.setAge(88);
		employee1.setMarried(true);
		employee1.setSalary(38.34);
		
		Employee employee2 = new Employee();
		employee2.setFirstName("kjkh");
		employee2.setLastName("kkljl");
		employee2.setAge(28);
		employee2.setMarried(true);
		employee2.setSalary(283.34);
		
		List<Employee> employeeArray = new ArrayList<>();
		employeeArray.add(employee);
		employeeArray.add(employee1);
		employeeArray.add(employee2);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		String jsonEMployee = objectMapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(employeeArray);
		System.out.println(jsonEMployee);
	}

}
