package PojoPayloadExample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UseBooking2 {

	public static void main(String[] args) throws JsonProcessingException {
		
		Bookings bookings = new Bookings();
		
		bookings.setDepartFrom("London");
		
		Travellers travellers1 = new Travellers();
		travellers1.setAge(29);
		travellers1.setFirstName("Amod");
		travellers1.setLastName("Mahajan");
		
		Travellers travellers2 = new Travellers();
		travellers2.setAge(35);
		travellers2.setFirstName("Rahul");
		travellers2.setLastName("Arora");
		
		List<Travellers> allTravellers = new LinkedList<Travellers>();
		allTravellers.add(travellers1);
		allTravellers.add(travellers2);
		
		bookings.setTravellers(allTravellers);
		
		
		Payment payment = new Payment();
		payment.setAccountName("AMod");
		payment.setCarNo(32423423);
		payment.setCvv(234);
		
		bookings.setPayment(payment);
		
ObjectMapper objectMapper = new ObjectMapper();
		
		String jsonEMployee = objectMapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(bookings);
		System.out.println(jsonEMployee);
		
		
	}
	
}
