package PojoPayloadExample;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Employee {
	
	private String firstName;
	private String lastName;
	private int age;
	private boolean married;
	private double salary;
	
	
	

}
