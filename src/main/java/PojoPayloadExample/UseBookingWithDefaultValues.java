package PojoPayloadExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UseBookingWithDefaultValues {

	public static void main(String[] args) throws JsonProcessingException {
		
		Bookings bookings = new Bookings();
		
		bookings.setDepartFrom("Bangalore");
		
ObjectMapper objectMapper = new ObjectMapper();
		
		String jsonEMployee = objectMapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(bookings);
		System.out.println(jsonEMployee);
	}
}
