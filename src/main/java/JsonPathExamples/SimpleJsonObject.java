package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class SimpleJsonObject {

	@Test
	public void simpleJson()
	{
		String json = "{\r\n" + 
				"  \"firsName\": \"Amod\",\r\n" + 
				"  \"lastName\": \"Mahajan\",\r\n" + 
				"  \"age\": 28,\r\n" + 
				"  \"married\": false,\r\n" + 
				"  \"salary\": 23.45\r\n" + 
				"}";
		
		JsonPath jsonPath = JsonPath.from(json);
		
		String firstName = jsonPath.getString("firsName");
		System.out.println(firstName);
		
		System.out.println(jsonPath.getInt("age"));
		System.out.println(jsonPath.getBoolean("married"));
		System.out.println(jsonPath.getDouble("salary"));
	}
	
}
