package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class FIndEx2 {

	@Test
	public void ex2()
	{
		String json = "{\r\n" + 
				"	\"data\": [\r\n" + 
				"		{\r\n" + 
				"			\"id\": 1,\r\n" + 
				"			\"first_name\": \"Lauraine\",\r\n" + 
				"			\"last_name\": \"Vearnals\",\r\n" + 
				"			\"email\": \"lvearnals0@last.fm\",\r\n" + 
				"			\"gender\": \"F\"\r\n" + 
				"		},\r\n" + 
				"		{\r\n" + 
				"			\"id\": 2,\r\n" + 
				"			\"first_name\": \"Chan\",\r\n" + 
				"			\"last_name\": \"Skittreal\",\r\n" + 
				"			\"email\": \"cskittreal1@twitter.com\",\r\n" + 
				"			\"gender\": \"M\"\r\n" + 
				"		}\r\n" + 
				"	]\r\n" + 
				"}";
		
		JsonPath jsonPath = JsonPath.from(json);
		
		System.out.println(jsonPath.getString("data.find{contains(it.first_name, 'Chan')}.first_name"));
	}
}
