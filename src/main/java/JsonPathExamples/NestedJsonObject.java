package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class NestedJsonObject {

	@Test
	public void simpleJson()
	{
		String json = "{\r\n" + 
				"  \"firsName\": \"Amod\",\r\n" + 
				"  \"lastName\": \"Mahajan\",\r\n" + 
				"  \"age\": 28,\r\n" + 
				"  \"married\": false,\r\n" + 
				"  \"salary\": 23.45,\r\n" + 
				"  \"address\" : {\r\n" + 
				"    \"city\" : \"ABC\",\r\n" + 
				"    \"state\" : \"XYZ\"\r\n" + 
				"  }\r\n" + 
				"}";
		
		JsonPath jsonPath = JsonPath.from(json);
		
		System.out.println(jsonPath.getString("address.city"));
	}
	
}
