package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class JsonArrayNested {

	@Test
	public void jsonPathEx()
	{
		String json = "[\r\n" + 
				"  [\r\n" + 
				"    {\r\n" + 
				"      \"firsName\": \"Amod\",\r\n" + 
				"      \"lastName\": \"Mahajan\",\r\n" + 
				"      \"age\": 28,\r\n" + 
				"      \"married\": false,\r\n" + 
				"      \"salary\": 23.45\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"firsName\": \"Amod1\",\r\n" + 
				"      \"lastName\": \"Mahajan1\",\r\n" + 
				"      \"age\": 29,\r\n" + 
				"      \"married\": false,\r\n" + 
				"      \"salary\": 24.45\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"]";
		
		JsonPath jsonPath = JsonPath.from(json);
		
	}
}
