package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class JsonArrayExample {
	
	@Test
	public void jsonArrayExample()
	{
		String json = "{\r\n" + 
				"  \"firsName\": \"Amod\",\r\n" + 
				"  \"lastName\": \"Mahajan\",\r\n" + 
				"  \"age\": 28,\r\n" + 
				"  \"married\": false,\r\n" + 
				"  \"salary\": 23.45,\r\n" + 
				"  \"address\": [\r\n" + 
				"    {\r\n" + 
				"      \"city\": \"ABC\",\r\n" + 
				"      \"state\": \"XYZ\",\r\n" + 
				"      \"def\": {\r\n" + 
				"        \"mob\": 12212\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"city\": \"RTY\",\r\n" + 
				"      \"state\": \"HJK\",\r\n" + 
				"      \"def\": {\r\n" + 
				"        \"mob\": 433\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		JsonPath jsonPath = JsonPath.from(json);
		
		System.out.println(jsonPath.getString("address.city"));
		System.out.println(jsonPath.getList("address.def.mob"));
		
		System.out.println(jsonPath.getString("address[1].city"));
	}

}
