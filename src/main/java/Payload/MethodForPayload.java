package Payload;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestLogSpecification;
import io.restassured.specification.RequestSpecification;

public class MethodForPayload {
	
	public String getCreateBoookingPayload(String firstName, String lastName, int totalPrice)
	{
		String payload = "{\r\n" + 
				"    \"firstname\" : \""+firstName+"\",\r\n" + 
				"    \"lastname\" : \""+lastName+"\",\r\n" + 
				"    \"totalprice\" : "+totalPrice+",\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}";
		
		return payload;
	}
	
	@Test
	public void createBookingDetailed()
	{

	//1. Configure request - RequestSpecification
	//RequestSpecification requestSpecification = RestAssured.given();
	
	//RequestLogSpecification requestLogSpecification = RestAssured.given().log();
	//requestSpecification = RestAssured.given().log().all();
	
	//RestAssured.given().log().all().header("Content-Type", "application/json");
	
	RestAssured
		.given()
		.log()
		.all()
		.header("Content-Type", "application/json")
		.body(getCreateBoookingPayload("amod", "mahajan", 111))
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
	
	
	// 2. Hit the request and get the response
		.post()
	
	//3. Assert/verify response
		.then()
		.log().all()
		.statusCode(200);
	}
	
	
}
