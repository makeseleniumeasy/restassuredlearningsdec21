package Payload;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestLogSpecification;
import io.restassured.specification.RequestSpecification;

public class UsigMapForNestedJSONObject {
	
	
	
	@Test
	public void createBookingDetailed()
	{

	//1. Configure request - RequestSpecification
	//RequestSpecification requestSpecification = RestAssured.given();
	
	//RequestLogSpecification requestLogSpecification = RestAssured.given().log();
	//requestSpecification = RestAssured.given().log().all();
	
	//RestAssured.given().log().all().header("Content-Type", "application/json");
	
		/*
		 * {
    "firstname" : "Jim",
    "lastname" : "Brown",
    "totalprice" : 111,
    "depositpaid" : true,
    "additionalneeds" : "Breakfast"
}'
		 */
		
	Map<String,Object> payload = new LinkedHashMap<>();
	payload.put("firstname", "Jim");
	payload.put("lastname", "Brown");
	payload.put("totalprice", 111);
	payload.put("depositpaid", true);
	payload.put("additionalneeds", "Breakfast");
	
	
	Map<String,Object> bookingDetailsData = new HashMap<>();
	bookingDetailsData.put("checkin", "2018-01-01");
	bookingDetailsData.put("checkout", "2019-01-01");
	
	payload.put("bookingdates", bookingDetailsData);
	
	
	
	// Address
	
	Map<String,Object> address = new HashMap<>();
	
	
	Map<String,String> addressDetails = new HashMap<>();
	addressDetails.put("city", "Bangalore");
	addressDetails.put("state", "KA");
	
	address.put("communication", addressDetails);
	addressDetails = new HashMap<>();
	addressDetails.put("city", "Bhopal");
	addressDetails.put("state", "MP");
	
	address.put("temp", addressDetails);
	
	payload.put("address", address);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	RestAssured
		.given()
		.log()
		.all()
		.header("Content-Type", "application/json")
		.body(payload)
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
	
	
	// 2. Hit the request and get the response
		.post()
	
	//3. Assert/verify response
		.then()
		.log().all()
		.statusCode(200);
	}
	
	
}
