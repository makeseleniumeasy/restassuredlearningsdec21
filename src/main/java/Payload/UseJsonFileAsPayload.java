package Payload;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestLogSpecification;
import io.restassured.specification.RequestSpecification;

public class UseJsonFileAsPayload {
	
	@Test
	public void createBookingDetailed()
	{	
	
		File jsonPayload = new File("C:\\Amod Mahajan\\Youtube\\API Batch Dec\\RestAssuredLearningsDec\\src\\main\\java\\Payload\\createBookingPayload.json");
		
	RestAssured
		.given()
		.log()
		.all()
		.header("Content-Type", "application/json")
		.body
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
	
	
	// 2. Hit the request and get the response
		.post()
	
	//3. Assert/verify response
		.then()
		.log().all()
		.statusCode(200);
	}
	
	
}
