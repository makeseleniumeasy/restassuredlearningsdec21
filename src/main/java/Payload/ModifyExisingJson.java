package Payload;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ModifyExisingJson {
	
	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		
		String someJson = "{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\"\r\n" + 
				"}";
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map jsonMap = objectMapper.readValue(someJson, Map.class);
		
		jsonMap.put("age", 28);
		
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMap));
		
ObjectNode jsonMap1 = objectMapper.readValue(someJson, ObjectNode.class);
		
		jsonMap1.put("married", false);
		
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMap1));
		
		
		
	}

}
