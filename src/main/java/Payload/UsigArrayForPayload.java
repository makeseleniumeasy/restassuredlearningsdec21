package Payload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestLogSpecification;
import io.restassured.specification.RequestSpecification;

public class UsigArrayForPayload {
	
	
	
	@Test
	public void createBookingDetailed()
	{

	//1. Configure request - RequestSpecification
	//RequestSpecification requestSpecification = RestAssured.given();
	
	//RequestLogSpecification requestLogSpecification = RestAssured.given().log();
	//requestSpecification = RestAssured.given().log().all();
	
	//RestAssured.given().log().all().header("Content-Type", "application/json");
	
		/*
		 * {
    "firstname" : "Jim",
    "lastname" : "Brown",
    "totalprice" : 111,
    "depositpaid" : true,
    "additionalneeds" : "Breakfast"
}'
		 */
		
	Map<String,Object> emp1 = new HashMap<>();
	emp1.put("firstname", "Jim");
	emp1.put("lastname", "Brown");
	emp1.put("totalprice", 111);
	emp1.put("depositpaid", true);
	emp1.put("additionalneeds", "Breakfast");
	
	Map<String,Object> emp2 = new HashMap<>();
	emp2.put("firstname", "Rahul");
	emp2.put("lastname", "Arora");
	emp2.put("totalprice", 111);
	emp2.put("depositpaid", true);
	emp2.put("additionalneeds", "Breakfast");
	
	
	List<Map<String,Object>> mobDetails = new ArrayList<>();
	
	Map<String,Object> firstMob = new HashMap<>();
	firstMob.put("number", 123);
	firstMob.put("type", "office");
	
	Map<String,Object> secondMob = new HashMap<>();
	secondMob.put("number", 456);
	secondMob.put("type", "Personal");
	
	mobDetails.add(firstMob);
	mobDetails.add(secondMob);
	
	emp2.put("mob", mobDetails);
	
	
	
	Map<String,Object> emp3 = new HashMap<>();
	emp3.put("firstname", "Manish");
	emp3.put("lastname", "Gupta");
	emp3.put("totalprice", 111);
	emp3.put("depositpaid", true);
	emp3.put("additionalneeds", "Breakfast");
	
	List<Map<String,Object>> employeeDetails = new ArrayList<>();
	employeeDetails.add(emp1);
	employeeDetails.add(emp2);
	employeeDetails.add(emp3);
		
	RestAssured
		.given()
		.log()
		.all()
		.header("Content-Type", "application/json")
		.body(employeeDetails)
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
	
	
	// 2. Hit the request and get the response
		.post()
	
	//3. Assert/verify response
		.then()
		.log().all()
		.statusCode(200);
	}
	
	
}
