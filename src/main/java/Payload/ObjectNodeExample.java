package Payload;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.restassured.RestAssured;

public class ObjectNodeExample {

	public static void main(String[] args) throws JsonProcessingException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		ObjectNode emp1 = objectMapper.createObjectNode();
		
		emp1.put("firstname", "Jim");
		emp1.put("lastname", "Brown");
		emp1.put("totalprice", 111);
		emp1.put("depositpaid", true);
		emp1.put("additionalneeds", "Breakfast");
		
		ArrayNode empDtails = objectMapper.createArrayNode();
		empDtails.add(emp1);
		
		String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(emp1);
		System.out.println(jsonString);
		
		
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(empDtails));
		
		
//		RestAssured
//		.given()
//		.log()
//		.all()
//		.header("Content-Type", "application/json")
//		.body(empDtails)
//		.baseUri("https://restful-booker.herokuapp.com/")
//		.basePath("booking")
//	
//	
//	// 2. Hit the request and get the response
//		.post()
//	
//	//3. Assert/verify response
//		.then()
//		.log().all()
//		.statusCode(200);
//		
		
	}
}
