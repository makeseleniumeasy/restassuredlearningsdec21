package Payload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ModifyExisingJson2 {
	
	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		
		String someJson = "[{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\"\r\n" + 
				"},\r\n" + 
				"{\r\n" + 
				"    \"firstname\" : \"Rahul\",\r\n" + 
				"    \"lastname\" : \"Arora\"\r\n" + 
				"}]";
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		List jsonMap = objectMapper.readValue(someJson, List.class);
		
		
		Map<String,Object> emp1Data = (Map<String, Object>) jsonMap.get(0);
		emp1Data.put("firstname", "Jimmy");
		emp1Data.remove("lastname");
		
		Map<String,String> emp3 = new HashMap<>();
		emp3.put("fn", "ds");
		emp3.put("ln", "dsfsd");
		
		jsonMap.add(emp3);
		
	
		
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMap));
		
//ObjectNode jsonMap1 = objectMapper.readValue(someJson, ObjectNode.class);
//		
//		jsonMap1.put("married", false);
//		
//		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMap1));
		
		
		
	}

}
