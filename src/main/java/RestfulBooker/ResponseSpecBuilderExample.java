package RestfulBooker;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class ResponseSpecBuilderExample {

RequestSpecification requestSpecification;
ResponseSpecification responseSpecification;
	
	@BeforeClass
	public void setup()
	{
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setBaseUri("https://restful-booker.herokuapp.com/");
		requestSpecBuilder.setBasePath("booking");
		requestSpecBuilder.addHeader("Content-Type", "application/json");
		requestSpecBuilder.log(LogDetail.ALL);
		requestSpecification = requestSpecBuilder.build();
		
		
		ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
		responseSpecBuilder.expectStatusCode(200);
		responseSpecBuilder.expectContentType(ContentType.JSON);
		responseSpecBuilder.expectResponseTime(Matchers.lessThan(10000L));
		responseSpecification = responseSpecBuilder.build();
		
		
	}
	
	
//	@Test
//	public void demo()
//	{
//		RestAssured.given(requestSpecification)
//		.body("")
//		..
//		.requestSpecification.
//	}
}
