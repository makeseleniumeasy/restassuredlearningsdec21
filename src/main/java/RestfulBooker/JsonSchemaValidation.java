package RestfulBooker;

import java.io.File;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;

public class JsonSchemaValidation {
	
	public static void main(String[] args) {
		RestAssured
		.given()
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/booking")
		.body("{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
		.contentType(ContentType.JSON)
		.post()
		.then()
		.log()
		.all()
		//.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("CreateBookingSchema.json"));
		.body(JsonSchemaValidator
				.matchesJsonSchema(
						new File(System.getProperty("user.dir")+"\\src\\main\\java\\DataFiles\\CreateBookingSchema.json")));
		
	}

}
