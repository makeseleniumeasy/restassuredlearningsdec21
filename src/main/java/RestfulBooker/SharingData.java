package RestfulBooker;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

// sharing data among test methods within same class
// sharing data among test methods within different testng class
public class SharingData {
	
	@Test
	public void createBooking(ITestContext context)
	{
		int bookingid = RestAssured
		.given()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("booking")
			.log()
			.all()
			.contentType(ContentType.JSON)
			.body("{\r\n" + 
					"    \"firstname\" : \"Jim\",\r\n" + 
					"    \"lastname\" : \"Brown\",\r\n" + 
					"    \"totalprice\" : 111,\r\n" + 
					"    \"depositpaid\" : true,\r\n" + 
					"    \"bookingdates\" : {\r\n" + 
					"        \"checkin\" : \"2018-01-01\",\r\n" + 
					"        \"checkout\" : \"2019-01-01\"\r\n" + 
					"    },\r\n" + 
					"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
					"}")
		.when()
			.post()
		.then()
			.extract()
			.jsonPath()
			.getInt("bookingid");
		
		System.out.println(bookingid);
		context.setAttribute("bookingId", bookingid);
		
	}
	
	@Test
	public void getBooking(ITestContext context)
	{
		int bookingId = (int) context.getAttribute("bookingId");
		RestAssured
		.given()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/" + bookingId)
		.log()
		.all()
		.when()
		.get()
		.then()
		.log()
		.all();
	}
}
