package RestfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestLogSpecification;
import io.restassured.specification.RequestSpecification;

public class CreateBookingDetailed {
	
	
	@Test
	public void createBookingDetailed()
	{

	//1. Configure request - RequestSpecification
	RequestSpecification requestSpecification = RestAssured.given();
	
	RequestLogSpecification requestLogSpecification = requestSpecification.log();
	requestSpecification = requestLogSpecification.all();
	
	requestSpecification.header("Content-Type", "application/json");
	
	requestSpecification.body("{\r\n" + 
			"    \"firstname\" : \"Jim\",\r\n" + 
			"    \"lastname\" : \"Brown\",\r\n" + 
			"    \"totalprice\" : 111,\r\n" + 
			"    \"depositpaid\" : true,\r\n" + 
			"    \"bookingdates\" : {\r\n" + 
			"        \"checkin\" : \"2018-01-01\",\r\n" + 
			"        \"checkout\" : \"2019-01-01\"\r\n" + 
			"    },\r\n" + 
			"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
			"}");
	
	
	requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
	requestSpecification.basePath("booking");
	
	
	// 2. Hit the request and get the response
	Response response = requestSpecification.post();
	
	//3. Assert/verify response
	ValidatableResponse validatableResponse = response.then();
	validatableResponse= validatableResponse.log().all();
	validatableResponse.statusCode(200);
	}
	
	
}
