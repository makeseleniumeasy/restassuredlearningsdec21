package RestfulBooker;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import com.google.common.collect.HashBiMap;

import io.restassured.RestAssured;
import io.restassured.http.Header;

public class HeadersExample {
	
	/*
	 * h1,v1
	 * h2,v2,
	 * h3,v3
	 */

	@Test
	public void addHeaders()
	{
		
		Map<String,String> allHeaders = new HashMap<>();
		allHeaders.put("h1", "v1");
		allHeaders.put("h2", "v2");
		allHeaders.put("h3", "v3");
		
		Header header = new Header("h4", "v4");
		
		RestAssured
			.given()
			.log()
			.all()
			.headers(allHeaders)
			.header(header)
			.get();
	}
}
